# File handler lib 
# .NET Core 2.1

# Architecrute
  - Straight architecrute level with factory, singleton as patterns. A couple of managers and services for both file formats(xml, binary). Also SR(single responsibility) were implemented for both of them.

# Weak
  - Using straight Car object to create, modify, convert and read operations.

# Strong 
  - libs flexibility : adding new file format or changing to any object from Car would cost small efforts. 

# In future could be:
  - New file formats
  - operating any objects inside the file