﻿using AsposeFileLibrary.Files.Aspose;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Shouldly;
using AsposeFileLibrary.Files.Models;
using AsposeFileLibrary.Common.Helpers;

namespace AsposeFileLibrary.Tests.Tests.FileTypes.Binary
{
    public class BinaryTests
    {
        [Fact]
        public async void ReadBinary()
        {
            var aspose = AsposeCommander.Instance;
            var assemblyPath = "AsposeFileLibrary.Tests.Assets.Test.binary";

            var fileBytes = await aspose.ReadFile(assemblyPath, Files.Models.ReadType.Embedded, FileFormat.Binary, this);
            
            var txt = Encoding.Default.GetString(fileBytes);

            txt.ShouldContain("PMOCCMOC");
        }

        [Fact]
        public async void ConvertToXml()
        {
            var aspose = AsposeCommander.Instance;
            var assemblyPath = "AsposeFileLibrary.Tests.Assets.Test.binary";

            var fileBytes = await aspose.ReadFile(assemblyPath, Files.Models.ReadType.Embedded, FileFormat.Binary, this);

            var converted = aspose.ConvertFileFormat(Files.Models.FileFormat.Binary, Files.Models.FileFormat.Xml, fileBytes);

            var txt = Encoding.ASCII.GetString(converted);

            txt.ShouldContain("<BrandName>Audi</BrandName>");
            txt.ShouldContain("<Document>");
        }

        [Fact]
        public async void ModifyAdd()
        {
            var aspose = AsposeCommander.Instance;
            var assemblyPath = "AsposeFileLibrary.Tests.Assets.Test.binary";
            var list = GetModificationList();

            var fileBytes = await aspose.ReadFile(assemblyPath, ReadType.Embedded, FileFormat.Binary, this);

            var converted = aspose.ModifyFile(fileBytes, list, FileFormat.Binary, ModificationType.Add);

            var txt = Encoding.Default.GetString(fileBytes);

            txt.ShouldContain("PMOCCMOC");
        }

        [Fact]
        public async void Modify()
        {
            var aspose = AsposeCommander.Instance;
            var assemblyPath = "AsposeFileLibrary.Tests.Assets.Test.binary";

            var fileBytes = await aspose.ReadFile(assemblyPath, Files.Models.ReadType.Embedded, FileFormat.Binary, this);

            var updateList = new List<Car> { new Car { BrandName = "Audi", Date = new DateTime(2015, 10, 10), Price = 50000 } };

            var modifyed = aspose.ModifyFile(fileBytes, updateList, FileFormat.Binary, ModificationType.Modify);

            var txt = Encoding.Default.GetString(modifyed);

            txt.ShouldContain("PMOCCMOC");
            txt.ShouldContain("Audi/50000");
        }

        [Fact]
        public async void ModifyDelete()
        {
            var aspose = AsposeCommander.Instance;
            var assemblyPath = "AsposeFileLibrary.Tests.Assets.Test.binary";

            var fileBytes = await aspose.ReadFile(assemblyPath, Files.Models.ReadType.Embedded, FileFormat.Binary, this);

            var deleteList = new List<Car> { new Car { BrandName = "Audi" } };

            var modifyed = aspose.ModifyFile(fileBytes, deleteList, FileFormat.Binary, ModificationType.Delete);

            var txt = Encoding.Default.GetString(modifyed);

            txt.ShouldContain("PMOCCMOC");
            txt.ShouldNotContain("Audi");
        }

        private List<Car> GetModificationList()
        {
            var list = new List<Car>
            {
                new Car
                {
                    BrandName = "BMW",
                    Date = new DateTime(2015, 10, 10),
                    Price = 200000
                }
            };

            return list;
        }
    }
}
