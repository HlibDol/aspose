﻿using AsposeFileLibrary.Common.Helpers;
using AsposeFileLibrary.Files.Aspose;
using AsposeFileLibrary.Files.Models;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace AsposeFileLibrary.Tests.Tests.FileTypes.Xml
{
    public class XmlTests
    {
        [Fact]
        public async void ReadLocal()
        {
            var aspose = AsposeCommander.Instance;

            var xml = await aspose.ReadFile("AsposeFileLibrary.Tests.Assets.Test.xml", ReadType.Embedded, FileFormat.Xml, this);

            var xmlStr = xml.ConvertToString();

            xmlStr.ShouldContain("<?xml version=");
        }

        [Fact]
        public async void Convert1()
        {
            var aspose = AsposeCommander.Instance;

            var xml = await aspose.ReadFile("AsposeFileLibrary.Tests.Assets.Test.xml", ReadType.Embedded, FileFormat.Xml, this);

            var converted = aspose.ConvertFileFormat(FileFormat.Xml, FileFormat.Binary, xml);

            converted = aspose.ConvertFileFormat(FileFormat.Binary, FileFormat.Xml, converted);

            var xmlTo = Encoding.Default.GetString(converted);

            xmlTo.ShouldContain("<?xml version=");
        }

        [Fact]
        public async void Convert2()
        {
            var aspose = AsposeCommander.Instance;

            var xml = await aspose.ReadFile("AsposeFileLibrary.Tests.Assets.Test.xml", ReadType.Embedded, FileFormat.Xml, this);

            var converted = aspose.ConvertFileFormat(FileFormat.Xml, FileFormat.Binary, xml);

            converted = aspose.ConvertFileFormat(FileFormat.Binary, FileFormat.Xml, converted);

            var xmlTo = Encoding.ASCII.GetString(converted);

            xmlTo.ShouldContain("<?xml version=");
        }

        [Fact]
        public async void Convert3()
        {
            var aspose = AsposeCommander.Instance;

            var xml = await aspose.ReadFile("AsposeFileLibrary.Tests.Assets.Test.xml", ReadType.Embedded, FileFormat.Xml, this);

            var converted = aspose.ConvertFileFormat(FileFormat.Xml, FileFormat.Binary, xml);

            converted = aspose.ConvertFileFormat(FileFormat.Binary, FileFormat.Xml, converted);

            var xmlTo = Encoding.ASCII.GetString(converted);

            xmlTo.ShouldContain("<?xml version=");
        }

        [Fact]
        public async void ModifyAdd()
        {
            var aspose = AsposeCommander.Instance;

            var xml = await aspose.ReadFile("AsposeFileLibrary.Tests.Assets.Test.xml", ReadType.Embedded, FileFormat.Xml, this);

            var list = new List<Car> { new Car { BrandName = "Mitsubishi", Date = new System.DateTime(1970, 10, 10), Price = 5000 }};

            var binary =  aspose.ModifyFile(xml, list, FileFormat.Xml, ModificationType.Add);

            var xmlTo = Encoding.ASCII.GetString(binary);

            xmlTo.ShouldContain("Mitsubishi");
        }

        [Fact]
        public async void Modify()
        {
            var aspose = AsposeCommander.Instance;

            var xml = await aspose.ReadFile("AsposeFileLibrary.Tests.Assets.Test.xml", ReadType.Embedded, FileFormat.Xml, this);

            var list = new List<Car> { new Car { BrandName = "BMW", Date = new System.DateTime(1970, 10, 10), Price = 5000 } };

            var binary = aspose.ModifyFile(xml, list, FileFormat.Xml, ModificationType.Modify);

            var xmlTo = Encoding.ASCII.GetString(binary);

            foreach (var item in list)
            {
                var carXml = ConstructXml(item);

                xmlTo.ShouldContain(carXml);
            }
        }

        [Fact]
        public async void ModifyDelete()
        {
            var aspose = AsposeCommander.Instance;

            var xml = await aspose.ReadFile("AsposeFileLibrary.Tests.Assets.Test.xml", ReadType.Embedded, FileFormat.Xml, this);

            var list = new List<Car> { new Car { BrandName = "BMV", Date = new System.DateTime(1970, 10, 10), Price = 5000 } };

            var binary = aspose.ModifyFile(xml, list, FileFormat.Xml, ModificationType.Delete);
        }

        private string ConstructXml(Car car)
        {
            return $"<Car><Date>{car.Date.ToShortDateString()}</Date><BrandName>{car.BrandName}</BrandName><Price>{car.Price}</Price></Car>";
        }
    }
}
