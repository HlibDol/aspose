﻿using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Files.FileTypes.Abstract
{
    public interface IFileModifyer
    {
        byte[] Modify(byte[] data, List<Car> updatedData);

        byte[] ModifyContent(byte[] data, List<Car> updatedData, ModificationType type);

        byte[] Add(byte[] data, List<Car> updatedData);

        byte[] Delete(byte[] data, List<Car> updatedData);
    }
}
