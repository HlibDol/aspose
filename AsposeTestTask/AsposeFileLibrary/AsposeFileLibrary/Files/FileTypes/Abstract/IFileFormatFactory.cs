﻿using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Files.FileTypes.Abstract
{
    internal interface IFileExtensionFactory
    {
        IFileConstructor GetFileConstructor(FileFormat format);
    }
}
