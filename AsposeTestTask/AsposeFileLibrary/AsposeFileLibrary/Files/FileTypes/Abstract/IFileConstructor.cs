﻿using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace AsposeFileLibrary.Files.FileTypes.Abstract
{
    internal interface IFileConstructor
    {
        byte[] Modify(byte[] data, List<Car> updatedData, ModificationType type);

        byte[] ConvertTo(FileFormat format, byte[] data);

        byte[] ConvertFrom(List<Car> data);

        Task<byte[]> Read(string path, ReadType readType, object assemblyObject = null);
    }
}
