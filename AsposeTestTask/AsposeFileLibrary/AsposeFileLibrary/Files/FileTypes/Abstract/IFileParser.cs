﻿using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Files.FileTypes.Abstract
{
    internal interface IFileParser
    {
        byte[] ParseToByteArray(List<Car> list);

        List<Car> ParseIncomingData(byte[] array);
    }
}
