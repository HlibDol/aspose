﻿using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AsposeFileLibrary.Files.FileTypes.Abstract
{
    public interface IFileReader
    {
        Task<byte[]> Read(string path, ReadType readType, object assemblyObject = null);

        Task<byte[]> ReadLocalFile(string path);

        byte[] GetEmbeddedResource(string path, object assemblyObject);

        Task<byte[]> ReadRemoteFile(string path);
    }
}
