﻿using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Files.FileTypes.Abstract
{
    public interface IFileConvertor
    {
        byte[] Convert(FileFormat to, byte[] data);

        byte[] ConvertFrom(List<Car> list);
    }
}
