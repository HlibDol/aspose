﻿using AsposeFileLibrary.Common.Exceptions;
using AsposeFileLibrary.Common.Helpers;
using AsposeFileLibrary.Files.FileTypes.Abstract;
using AsposeFileLibrary.Files.FileTypes.Binary.Abstract;
using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AsposeFileLibrary.Common.Constants;

namespace AsposeFileLibrary.Files.FileTypes.Binary
{
    internal class BinaryService : IBinaryService
    {
        private readonly IFileExtensionFactory _fileFactory = null;
        private readonly IBinaryModifyer _binaryModifyer = null;
        private readonly IBinaryReader _reader = null;
        private readonly IBinaryConvertor _binaryConvertor = null;

        public BinaryService(IFileExtensionFactory fileFactory, IBinaryModifyer binaryModifyer, IBinaryReader reader, IBinaryConvertor binaryConvertor)
        {
            _binaryConvertor = binaryConvertor;
            _reader = reader;   
            _binaryModifyer = binaryModifyer;
            _fileFactory = fileFactory;
        }

        public byte[] ConvertFrom(List<Car> data)
        {
            return _binaryConvertor.ConvertFrom(data);
        }

        public byte[] ConvertTo(FileFormat format, byte[] data)
        {
            return _binaryConvertor.Convert(format, data);
        }

        public async Task<byte[]> Read(string path, ReadType readType, object assemblyObject = null)
        {
            return await _reader.Read(path, readType, assemblyObject);
        }

        public byte[] Modify(byte[] data, List<Car> updatedData, ModificationType type)
        {
            return _binaryModifyer.ModifyContent(data, updatedData, type);
        }
    }
}
