﻿using AsposeFileLibrary.Files.FileTypes.Binary.Abstract;
using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsposeFileLibrary.Files.FileTypes.Binary
{
    internal class BinaryModifyer : IBinaryModifyer
    {
        private IBinaryParser _binaryParser = null;

        public BinaryModifyer(IBinaryParser binaryParser)
        {
            _binaryParser = binaryParser;
        }

        public byte[] Add(byte[] data, List<Car> updatedData)
        {
            var obj = _binaryParser.ParseIncomingData(data);

            obj.AddRange(updatedData);

            return _binaryParser.ParseToByteArray(obj);
        }

        public byte[] Delete(byte[] data, List<Car> updatedData)
        {
            var obj = _binaryParser.ParseIncomingData(data);

            foreach (var item in updatedData)
            {
                var changedCar = obj.Where(x =>
                {
                    var predicate = x.BrandName.Equals(item.BrandName);

                    if (predicate)
                    {
                        x.Price = item.Price;
                        x.Date = item.Date;
                    }

                    return predicate;
                }).FirstOrDefault();

                obj.Remove(changedCar);
            }

            return _binaryParser.ParseToByteArray(obj);
        }

        public byte[] Modify(byte[] data, List<Car> updatedData)
        {
            var changedList = new List<Car>();
            var binaryContent = _binaryParser.ParseIncomingData(data);

            foreach (var item in updatedData)
            {
                var changedCar = binaryContent.Where(x =>
                {
                    var predicate = x.BrandName.Equals(item.BrandName);

                    if (predicate)
                    {
                        x.Price = item.Price;
                        x.Date = item.Date;
                    }

                    return predicate;
                }).FirstOrDefault();

                changedList.Add(changedCar);
            }

            return _binaryParser.ParseToByteArray(changedList);
        }

        public byte[] ModifyContent(byte[] data, List<Car> updatedData, ModificationType modificationType)
        {
            byte[] returnData = null;

            if (modificationType == ModificationType.Add)
            {
                returnData = Add(data, updatedData);
            }
            else if (modificationType == ModificationType.Delete)
            {
                returnData = Delete(data, updatedData);
            }
            else if (modificationType == ModificationType.Modify)
            {
                returnData = Modify(data, updatedData);
            }
            else
            {
                throw new NotImplementedException("Your modification type have not implemented yet.");
            }

            return returnData;
        }
    }
}
