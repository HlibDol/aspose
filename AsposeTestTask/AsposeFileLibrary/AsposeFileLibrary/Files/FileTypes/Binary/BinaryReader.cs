﻿using AsposeFileLibrary.Common.Exceptions;
using AsposeFileLibrary.Common.Helpers;
using AsposeFileLibrary.Files.FileTypes.Abstract;
using AsposeFileLibrary.Files.FileTypes.Binary.Abstract;
using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsposeFileLibrary.Files.FileTypes.Binary
{
    internal class BinaryReader : IBinaryReader
    {
        public async Task<byte[]> Read(string path, ReadType readType, object assemblyObject = null)
        {
            if(readType == ReadType.Embedded)
            {
                return GetEmbeddedResource(path, assemblyObject);
            }
            else if(readType == ReadType.Local)
            {
                return await ReadLocalFile(path);
            }
            else if(readType == ReadType.Remote)
            {
                return await ReadRemoteFile(path);
            }
            else
            {
                throw new ImplementationException("This type is not implemented yet.");
            }
        }

        public async Task<byte[]> ReadLocalFile(string path)
        {
            if (File.Exists(path))
            {
                return await File.ReadAllBytesAsync(path);
            }
            else
            {
                throw new NoSuchFileException(@"There is no such file or directory found. :\");
            }
        }

        public byte[] GetEmbeddedResource(string path, object assemblyObject)
        {
            var binaryData = FileHelper.GetDataFromResource(path, assemblyObject);

            return ConvertBinaryToByteArray(binaryData);
        }

        public async Task<byte[]> ReadRemoteFile(string path)
        {
            var cancellation = new CancellationTokenSource(TimeSpan.FromSeconds(1));
            cancellation.CancelAfter(20000);

            var httpClient = new HttpClient();
            HttpResponseMessage response = null;

            try
            {
                response = await httpClient.GetAsync(path, cancellation.Token);
            }
            catch (TaskCanceledException)
            {
                throw new Common.Exceptions.TimeoutException("The file was donwloading more than 20 seconds.");
            }
            finally
            {
                cancellation.Dispose();
            }

            var content = await response.Content.ReadAsStringAsync();

            return ConvertBinaryToByteArray(content);
        }

        private byte[] ConvertBinaryToByteArray(string binaryString)
        {
            return Enumerable.Range(0, int.MaxValue / 8)
                          .Select(i => i * 8)
                          .TakeWhile(i => i < binaryString.Length)
                          .Select(i => binaryString.Substring(i, 8))
                          .Select(s => Convert.ToByte(s, 2))
                          .ToArray();
        }
    }
}
