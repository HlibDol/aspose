﻿using AsposeFileLibrary.Files.FileTypes.Abstract;
using AsposeFileLibrary.Files.FileTypes.Binary.Abstract;
using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Files.FileTypes.Binary
{
    internal class BinaryConvertor : IBinaryConvertor
    {
        private readonly IBinaryParser _binaryParser = null;
        private readonly IFileExtensionFactory _fileFactory = null;

        public BinaryConvertor(IBinaryParser binaryParser, IFileExtensionFactory filefactory)
        {
            _fileFactory = filefactory;
            _binaryParser = binaryParser;
        }

        public byte[] Convert(FileFormat to, byte[] data)
        {
            var convertTo = _fileFactory.GetFileConstructor(to);

            var objs = _binaryParser.ParseIncomingData(data);

            var convertedObjs = convertTo.ConvertFrom(objs);

            return convertedObjs;
        }

        public byte[] ConvertFrom(List<Car> list)
        {
            return _binaryParser.ParseToByteArray(list);
        }
    }
}
