﻿using AsposeFileLibrary.Common.Helpers;
using AsposeFileLibrary.Files.FileTypes.Binary.Abstract;
using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static AsposeFileLibrary.Common.Constants;

namespace AsposeFileLibrary.Files.FileTypes.Binary
{
    internal class BinaryParser : IBinaryParser
    {
        public List<Car> ParseIncomingData(byte[] array)
        {
            var utf = Encoding.Default.GetString(array);

            if(!utf.Contains(";"))
            {
                throw new ArgumentException("binary file should contain ; as separator");
            }

            var cars = new List<Car>();

            var strData = utf.Split(";");
            var headerData = strData[0];
            var carsCount = strData[1];

            var carObjs = strData[2].Split("|").ToList();

            foreach (var item in carObjs)
            {
                if (string.IsNullOrEmpty(item))
                {
                    continue;
                }

                var carObj = item.Split(PropSeparator).ToList();

                var date = carObj[0].TryParseToDateTime();
                var price = carObj[3].TryParseToInt();

                var car = new Car
                {
                    Date = date,
                    BrandName = carObj[2],
                    Price = price
                };

                cars.Add(car);
            }

            return cars;
        }

        public byte[] ParseToByteArray(List<Car> cars)
        {
            var combinedArray = default(byte[]);

            var splitter = Encoding.Default.GetBytes(";");
            var carSep = Encoding.Default.GetBytes("|");
            var header = Encoding.Default.GetBytes(BinaryHeader);
            var recordsCount = Encoding.Default.GetBytes(cars.Count.ToString());

            combinedArray = CombineByteArrays(header, splitter, recordsCount, splitter);

            foreach (var car in cars)
            {
                var date = Encoding.Default.GetBytes(car.Date.ToShortDateString() + PropSeparator);
                var brandName = Encoding.Default.GetBytes(car.BrandName + PropSeparator);
                var brandLength = Encoding.Default.GetBytes(car.BrandName.Length.ToString() + PropSeparator);
                var price = Encoding.Default.GetBytes(car.Price.ToString() + ObjectSeparator);

                combinedArray = CombineByteArrays(combinedArray, date, brandLength, brandName, price);
            }

            return combinedArray;
        }

        private byte[] CombineByteArrays(params byte[][] arrays)
        {
            var combined = new byte[arrays.Sum(a => a.Length)];
            var offset = 0;

            foreach (byte[] array in arrays)
            {
                Buffer.BlockCopy(array, 0, combined, offset, array.Length);

                offset += array.Length;
            }

            return combined;
        }
    }
}
