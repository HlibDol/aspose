﻿using AsposeFileLibrary.Common.Exceptions;
using AsposeFileLibrary.Files.FileTypes.Xml.Abstract;
using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AsposeFileLibrary.Files.FileTypes.Xml
{
    internal class XmlModifyer : IXmlModifyer
    {
        private readonly IXmlParser _xmlParser = null;

        public XmlModifyer(IXmlParser xmlParser)
        {
            _xmlParser = xmlParser;
        }

        public byte[] ModifyContent(byte[] data, List<Car> updatedData, ModificationType modificationType)
        {
            byte[] returnData = null;

            if (modificationType == ModificationType.Add)
            {
                returnData = Add(data, updatedData);
            }
            else if (modificationType == ModificationType.Delete)
            {
                returnData = Delete(data, updatedData);
            }
            else if (modificationType == ModificationType.Modify)
            {
                returnData = Modify(data, updatedData);
            }
            else
            {
                throw new NotImplementedException("Your modification type have not implemented yet.");
            }

            return returnData;
        }

        public byte[] Add(byte[] data, List<Car> updatedData)
        {
            var obj = _xmlParser.ParseIncomingData(data);

            obj.AddRange(updatedData);

            return _xmlParser.ParseToByteArray(obj);
        }

        public byte[] Delete(byte[] data, List<Car> updatedData)
        {
            return DeleteContent(data, updatedData);
        }

        public byte[] Modify(byte[] data, List<Car> updatedData)
        {
            var utf = Encoding.Default.GetString(data);

            if (!utf.Contains("<?xml"))
            {
                throw new WrongFileFormatException("The data you provide was in a different format. Set the correct one.");
            }

            return ModifyContent(data, updatedData);
        }

        private byte[] ModifyContent(byte[] data, List<Car> updates)
        {
            var changedList = new List<Car>();
            var xmlContent = _xmlParser.ParseIncomingData(data);

            foreach (var item in updates)
            {
                var changedCar = xmlContent.Where(x =>
                {
                    var predicate = x.BrandName.Equals(item.BrandName);

                    if (predicate)
                    {
                        x.Price = item.Price;
                        x.Date = item.Date;
                    }

                    return predicate;
                }).FirstOrDefault();

                changedList.Add(changedCar);
            }

            return _xmlParser.ParseToByteArray(changedList);
        }

        private byte[] DeleteContent(byte[] data, List<Car> cars)
        {
            return null;
        }
    }
}
