﻿using AsposeFileLibrary.Files.FileTypes.Abstract;
using AsposeFileLibrary.Files.FileTypes.Xml.Abstract;
using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Files.FileTypes.Xml
{
    internal class XmlConvertor : IXmlConvertor
    {
        private readonly IXmlParser _xmlParser = null;
        private readonly IFileExtensionFactory _fileFactory = null;

        public XmlConvertor(IXmlParser xmlParser, IFileExtensionFactory fileFactory)
        {
            _fileFactory = fileFactory;
            _xmlParser = xmlParser;
        }

        public byte[] Convert(FileFormat to, byte[] data)
        {
            var convertTo = _fileFactory.GetFileConstructor(to);

            var objs = _xmlParser.ParseIncomingData(data);

            var convertedObjs = convertTo.ConvertFrom(objs);

            return convertedObjs;
        }

        public byte[] ConvertFrom(List<Car> list)
        {
            return _xmlParser.ParseToByteArray(list);
        }
    }
}
