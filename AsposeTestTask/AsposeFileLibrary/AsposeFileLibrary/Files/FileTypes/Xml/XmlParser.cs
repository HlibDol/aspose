﻿using AsposeFileLibrary.Common.Helpers;
using AsposeFileLibrary.Files.FileTypes.Xml.Abstract;
using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace AsposeFileLibrary.Files.FileTypes.Xml
{
    internal class XmlParser : IXmlParser
    {
        public List<Car> ParseIncomingData(byte[] array)
        {
            var xml = Encoding.Default.GetString(array);

            xml = RemoveXmlPreamble(xml);

            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);

            var listOfCars = new List<Car>();
            var document = xmlDoc.SelectSingleNode("Document");

            foreach (XmlNode node in document.ChildNodes)
            {
                var xmlDate = node.SelectSingleNode("Date").InnerXml;
                var brand = node.SelectSingleNode("BrandName").InnerXml;
                var xmlPrice = node.SelectSingleNode("Price").InnerXml;

                var date = xmlDate.TryParseToDateTime();
                var price = xmlPrice.TryParseToInt();

                var carObj = new Car
                {
                    Date = date,
                    BrandName = brand,
                    Price = price
                };

                listOfCars.Add(carObj);
            }

            return listOfCars;
        }

        public byte[] ParseToByteArray(List<Car> list)
        {
            var dom = new XmlDocument();
            var xml = FileHelper.GetDataFromResource("AsposeFileLibrary.Files.FileTypes.Xml.Assets.BaseXml.xml", this);
            xml = RemoveXmlPreamble(xml);

            dom.LoadXml(xml);

            var document = dom.SelectSingleNode("Document");

            foreach (var car in list)
            {
                var rootCar = dom.CreateElement("Car");
                var price = dom.CreateElement("Price");
                var date = dom.CreateElement("Date");
                var brand = dom.CreateElement("BrandName");

                price.InnerXml = car.Price.ToString();
                brand.InnerXml = car.BrandName;
                date.InnerXml = car.Date.ToString("dd.MM.yyyy");

                rootCar.AppendChild(date);
                rootCar.AppendChild(brand);
                rootCar.AppendChild(price);

                document.AppendChild(rootCar);
            }

            return Encoding.Default.GetBytes(dom.OuterXml);
        }

        private string RemoveXmlPreamble(string xml)
        {
            var _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
            if (xml.StartsWith(_byteOrderMarkUtf8))
            {
                xml = xml.Remove(0, _byteOrderMarkUtf8.Length);
            }

            return xml;
        }
    }
}
