﻿using AsposeFileLibrary.Common.Exceptions;
using AsposeFileLibrary.Common.Helpers;
using AsposeFileLibrary.Files.FileTypes.Abstract;
using AsposeFileLibrary.Files.FileTypes.Xml.Abstract;
using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AsposeFileLibrary.Files.FileTypes.Xml
{
    internal class XmlService : IXmlService
    {
        private readonly IFileExtensionFactory _fileFactory = null;
        private readonly IXmlReader _xmlReader = null;
        private readonly IXmlModifyer _xmlModifyer = null;
        private readonly IXmlConvertor _xmlConvertor = null;

        public XmlService(IFileExtensionFactory fileFactory, IXmlReader xmlReader, IXmlModifyer xmlModifyer, IXmlConvertor xmlConvertor)
        {
            _xmlConvertor = xmlConvertor;
            _xmlModifyer = xmlModifyer;
            _xmlReader = xmlReader;
            _fileFactory = fileFactory;
        }

        public byte[] ConvertFrom(List<Car> data)
        {
            return _xmlConvertor.ConvertFrom(data);
        }

        public byte[] ConvertTo(FileFormat format, byte[] data)
        {
            return _xmlConvertor.Convert(format, data);
        }

        public byte[] Modify(byte[] data, List<Car> updatedData, ModificationType type)
        {
            return _xmlModifyer.ModifyContent(data, updatedData, type);
        }

        public async Task<byte[]> Read(string path, ReadType readType, object assemblyObject = null)
        {
            return await _xmlReader.Read(path, readType, assemblyObject);
        }
    }
}