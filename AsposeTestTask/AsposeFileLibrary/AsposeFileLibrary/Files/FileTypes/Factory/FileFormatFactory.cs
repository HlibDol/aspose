﻿using AsposeFileLibrary.Files.FileTypes.Abstract;
using AsposeFileLibrary.src.Components.DependencyInjection;
using AsposeFileLibrary.Files.FileTypes.Xml.Abstract;
using AsposeFileLibrary.Files.FileTypes.Binary.Abstract;
using AsposeFileLibrary.Files.Models;
using AsposeFileLibrary.Common.Exceptions;

namespace AsposeFileLibrary.Files.FileTypes.Factory
{
    internal class FileExtensionFactory : IFileExtensionFactory
    {
        public IFileConstructor GetFileConstructor(FileFormat extension)
        {
            if (extension == FileFormat.Xml)
            {
                return DILoader.TryResolve<IXmlService>();
            }
            else if (extension == FileFormat.Binary)
            {
                return DILoader.TryResolve<IBinaryService>();
            }
            else
            {
                throw new ImplementationException("File extension you provided isn`t supported. Try new one.");
            }
        }
    }
}
