﻿using AsposeFileLibrary.Common.Helpers;
using AsposeFileLibrary.Files.Services.Abstract;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsposeFileLibrary.Files.Services
{
    /// <summary>
    /// Were used in older version
    /// </summary>
    internal class FileReaderService : IFileReaderService
    {
        public async Task<byte[]> ReadLocalFile(string path)
        {
            if (File.Exists(path))
            {
                return await File.ReadAllBytesAsync(path);
            }
            else
            {
                throw new FileNotFoundException(@"There is no such file or directory found. :\");
            }
        }

        public byte[] GetEmbeddedResource(string path, object assemblyObject)
        {
            var binaryData = FileHelper.GetDataFromResource(path, assemblyObject);

            return Encoding.Default.GetBytes(binaryData);
        }

        public async Task<byte[]> ReadRemoteFile(string path)
        {
            var cancellation = new CancellationTokenSource(TimeSpan.FromSeconds(1));
            cancellation.CancelAfter(20000);

            var httpClient = new HttpClient();
            HttpResponseMessage response = null;

            try
            {
                response = await httpClient.GetAsync(path, cancellation.Token);
            }
            catch (TaskCanceledException)
            {
                throw new Exception("The file was donwloading more than 20 seconds.");
            }
            finally
            {
                cancellation.Dispose();
            }
           
            return await response.Content.ReadAsByteArrayAsync();
        }
    }
}
