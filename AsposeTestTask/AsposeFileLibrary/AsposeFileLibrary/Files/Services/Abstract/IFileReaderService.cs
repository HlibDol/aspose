﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AsposeFileLibrary.Files.Services.Abstract
{
    internal interface IFileReaderService
    {
        Task<byte[]> ReadLocalFile(string path);

        Task<byte[]> ReadRemoteFile(string path);

        byte[] GetEmbeddedResource(string path, object assemblyObject);
    }
}
