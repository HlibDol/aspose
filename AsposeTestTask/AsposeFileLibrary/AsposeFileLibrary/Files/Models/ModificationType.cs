﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Files.Models
{
    public enum ModificationType
    {
        Modify = 0,
        Add = 1,
        Delete = 2
    }
}
