﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Files.Models
{
    public enum ReadType
    {
        Embedded = 0,
        Remote = 1,
        Local = 2
    }
}
