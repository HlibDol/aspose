﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Files.Models
{
    public class Car
    {
        public DateTime Date { get; set; }

        public string BrandName { get; set; }

        public int Price { get; set; }
    }
}
