﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Files.Models
{
    public enum FileFormat
    {
        Xml = 1,
        Binary = 2
    }
}
