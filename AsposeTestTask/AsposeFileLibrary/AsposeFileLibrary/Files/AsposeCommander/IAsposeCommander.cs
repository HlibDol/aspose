﻿using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AsposeFileLibrary.Files.Aspose
{
    public interface IAsposeCommander
    {
        Task<byte[]> ReadFile(string path, ReadType type, FileFormat format, object assemblyObject);

        byte[] ModifyFile(byte[] data, List<Car> updatedData, FileFormat fileExtension, ModificationType modificationType);

        byte[] ConvertFileFormat(FileFormat from, FileFormat to, byte[] data);

        void Configure();
    }
}
