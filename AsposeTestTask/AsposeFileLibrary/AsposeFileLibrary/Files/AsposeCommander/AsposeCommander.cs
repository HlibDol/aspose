﻿using AsposeFileLibrary.Common.Exceptions;
using AsposeFileLibrary.Files.Managers;
using AsposeFileLibrary.Files.Managers.Abstract;
using AsposeFileLibrary.Files.Models;
using AsposeFileLibrary.src.Components.DependencyInjection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsposeFileLibrary.Files.Aspose
{
    public class AsposeCommander : IAsposeCommander
    {
        public static AsposeCommander Instance
        {
            get
            {
                _instance = _instance ?? new AsposeCommander();

                return _instance;
            }
        }

        private static AsposeCommander _instance;
        private IFileManager _fileManager;

        private AsposeCommander()
        {
            Configure();
        }

        /// <summary>
        /// Configure the library
        /// </summary>
        public void Configure()
        {
            DILoader.Load();

            _fileManager = DILoader.Resolve<IFileManager>();
        }

        /// <summary>
        /// Convert from one extension to another
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="data"></param>
        /// <returns>byte data of "to" format</returns>
        public byte[] ConvertFileFormat(FileFormat from, FileFormat to, byte[] data)
        {
            if(data == null || data.Length == 0)
            {
                throw new LibraryRestrictionException("The data you provided is empty.");
            }

            return _fileManager.ConvertFileFormat(from, to, data);
        }

        /// <summary>
        /// Modify the file`s data
        /// </summary>
        /// <param name="data">data to modify</param>
        /// <param name="updatedData">data you wanna update</param>
        /// <param name="fileExtension">file extension of current file</param>
        /// <returns>byte array with modified data</returns>
        public byte[] ModifyFile(byte[] data, List<Car> updatedData, FileFormat fileExtension, ModificationType modificationType)
        {
            if (data == null || updatedData == null)
            {
                return null;
            }

            return _fileManager.ModifyFileContent(data, updatedData, fileExtension, modificationType);
        }

        /// <summary>
        /// Read data from remote and local source
        /// </summary>
        /// <param name="path">path to your file</param>
        /// <param name="type">read type you wanna use</param>
        /// <param name="assemblyObject">may be null if read type != embedded</param>
        /// <returns>byte array</returns>
        public async Task<byte[]> ReadFile(string path, ReadType type, FileFormat format, object assemblyObject = null)
        {
            if (string.IsNullOrEmpty(path))
            {
                return null;
            }

            return await _fileManager.ReadFileData(path, type, format, assemblyObject);
        }
    }
}
