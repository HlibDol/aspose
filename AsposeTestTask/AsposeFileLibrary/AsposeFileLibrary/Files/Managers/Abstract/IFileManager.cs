﻿using AsposeFileLibrary.Files.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsposeFileLibrary.Files.Managers.Abstract
{
    internal interface IFileManager
    {
        Task<byte[]> ReadFileData(string path, ReadType readType, FileFormat fileFormat, object assemblyObject);

        byte[] ModifyFileContent(byte[] data, List<Car> updatedData, FileFormat fileExtension, ModificationType modificationType);

        byte[] ConvertFileFormat(FileFormat from, FileFormat to, byte[] data);
    }
}
