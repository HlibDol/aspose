﻿using AsposeFileLibrary.Common.Exceptions;
using AsposeFileLibrary.Files.FileTypes.Abstract;
using AsposeFileLibrary.Files.Managers.Abstract;
using AsposeFileLibrary.Files.Models;
using AsposeFileLibrary.Files.Services.Abstract;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsposeFileLibrary.Files.Managers
{
    internal class FileManager : IFileManager
    {
        private readonly IFileReaderService _fileReader = null;
        private readonly IFileExtensionFactory _fileFactory = null;

        public FileManager(IFileReaderService fileReader, IFileExtensionFactory fileFactory)
        {
            _fileReader = fileReader;
            _fileFactory = fileFactory;
        }

        public byte[] ConvertFileFormat(FileFormat from, FileFormat to, byte[] data)
        {
            if(from == to)
            {
                return data;
            }

            var fileType = _fileFactory.GetFileConstructor(from);

            return fileType.ConvertTo(to, data);
        }

        public byte[] ModifyFileContent(byte[] data, List<Car> updatedData, FileFormat fileExtension, ModificationType modificationType)
        {
            if(updatedData == null || updatedData.Count == 0)
            {
                return data;
            }

            var fileType = _fileFactory.GetFileConstructor(fileExtension);

            return fileType.Modify(data, updatedData, modificationType);
        }

        public async Task<byte[]> ReadFileData(string path, ReadType readType, FileFormat fileFormat, object assemblyObject = null)
        {
            if (readType == ReadType.Embedded && assemblyObject == null)
            {
                throw new LibraryRestrictionException("You have to pass assembly object if you want to read embedded resources.");
            }

            var fileType = _fileFactory.GetFileConstructor(fileFormat);

            return await fileType.Read(path, readType, assemblyObject);
        }
    }
}