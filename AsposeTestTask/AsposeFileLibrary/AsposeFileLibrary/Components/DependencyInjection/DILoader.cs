﻿using AsposeFileLibrary.Files.FileTypes.Abstract;
using AsposeFileLibrary.Files.FileTypes.Binary;
using AsposeFileLibrary.Files.FileTypes.Binary.Abstract;
using AsposeFileLibrary.Files.FileTypes.Factory;
using AsposeFileLibrary.Files.FileTypes.Xml;
using AsposeFileLibrary.Files.FileTypes.Xml.Abstract;
using AsposeFileLibrary.Files.Managers;
using AsposeFileLibrary.Files.Managers.Abstract;
using AsposeFileLibrary.Files.Services;
using AsposeFileLibrary.Files.Services.Abstract;
using Autofac;

namespace AsposeFileLibrary.src.Components.DependencyInjection
{
    internal static class DILoader
    {
        internal static IContainer Container { get; set; }

        internal static void Load()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<FileExtensionFactory>().As<IFileExtensionFactory>();
            builder.RegisterType<BinaryService>().As<IBinaryService>();
            builder.RegisterType<XmlService>().As<IXmlService>();
            builder.RegisterType<FileManager>().As<IFileManager>();
            builder.RegisterType<FileReaderService>().As<IFileReaderService>();
            builder.RegisterType<FileExtensionFactory>().As<IFileExtensionFactory>();
            builder.RegisterType<XmlConvertor>().As<IXmlConvertor>();
            builder.RegisterType<XmlModifyer>().As<IXmlModifyer>();
            builder.RegisterType<XmlConvertor>().As<IXmlConvertor>();
            builder.RegisterType<XmlParser>().As<IXmlParser>();
            builder.RegisterType<XmlReader>().As<IXmlReader>();
            builder.RegisterType<BinaryConvertor>().As<IBinaryConvertor>();
            builder.RegisterType<BinaryModifyer>().As<IBinaryModifyer>();
            builder.RegisterType<BinaryParser>().As<IBinaryParser>();
            builder.RegisterType<BinaryReader>().As<IBinaryReader>();

            Container = builder.Build();
        }

        internal static T Resolve<T>()
        {
            return Container.Resolve<T>();
        }

        internal static T TryResolve<T>()
        {
            Container.TryResolve<T>(out T obj);

            return obj;
        }
    }
}
