﻿using AsposeFileLibrary.src.Components.DependencyInjection;

namespace AsposeFileLibrary.src.Components
{
    internal static class Startup
    {
        public static void Initialize()
        {
            DILoader.Load();
        }
    }
}
