﻿namespace AsposeFileLibrary.Common
{
    internal static class Constants
    {
        internal const string Http = "http";
        internal const string Xml = "xml";
        internal const string Bin = ".bin";
        internal const string Dat = ".dat";

        internal const string BinaryHeader = "PMOCCMOC";

        internal const string ObjectSeparator = "|";
        internal const string PropSeparator = "/";
    }
}
