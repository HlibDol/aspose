﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Common.Exceptions
{
    public class NoSuchFileException : AsposeCommonException
    {
        public NoSuchFileException(string exceptionMessage) : base(exceptionMessage)
        {
        }

        public NoSuchFileException() : base()
        {
        }
    }
}
