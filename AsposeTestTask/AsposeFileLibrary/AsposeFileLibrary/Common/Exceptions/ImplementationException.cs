﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Common.Exceptions
{
    public class ImplementationException : AsposeCommonException
    {
        public ImplementationException(string exceptionMessage) : base(exceptionMessage)
        {
        }

        public ImplementationException() : base()
        {
        }
    }
}
