﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Common.Exceptions
{
    public class LibraryRestrictionException : AsposeCommonException
    {
        public LibraryRestrictionException(string exceptionMessage) : base(exceptionMessage)
        {
        }

        public LibraryRestrictionException() : base()
        {
        }
    }
}
