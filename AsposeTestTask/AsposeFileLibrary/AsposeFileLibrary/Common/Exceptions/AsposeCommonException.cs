﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Common.Exceptions
{
    public class AsposeCommonException : Exception
    {
        public AsposeCommonException(string ex) : base(ex)
        {
        }

        public AsposeCommonException() : base()
        {
        }
    }
}
