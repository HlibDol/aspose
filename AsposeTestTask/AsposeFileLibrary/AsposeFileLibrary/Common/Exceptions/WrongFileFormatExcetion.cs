﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Common.Exceptions
{
    public class WrongFileFormatException : AsposeCommonException
    {
        public WrongFileFormatException(string exceptionMessage) : base(exceptionMessage)
        {
        }

        public WrongFileFormatException() : base()
        {
        }
    }
}
