﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeFileLibrary.Common.Exceptions
{
    public class TimeoutException : AsposeCommonException
    {
        public TimeoutException(string ex) : base(ex)
        {
        }

        public TimeoutException() : base()
        {
        }
    }
}
