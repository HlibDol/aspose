﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Configuration;

namespace AsposeFileLibrary.Common.Helpers
{
    public static class FileHelper
    {
        public static string GetDataFromResource(string resourceName, object assemblyObject)
        {
            if (string.IsNullOrEmpty(resourceName))
            {
                throw new ArgumentException("empty path");
            }

            if(assemblyObject == null)
            {
                throw new ArgumentException("Assembly object is null");
            }

            using (var stream = Assembly.GetAssembly(assemblyObject.GetType()).GetManifestResourceStream(resourceName))
            {
                if(stream == null)
                {
                    throw new ArgumentException("Wrong resource name");
                }

                var memoryStream = new MemoryStream();
                stream.CopyTo(memoryStream);

                var bytes = memoryStream.ToArray();

                return Encoding.Default.GetString(bytes, 0, bytes.Length);
            }
        }
    }
}
