﻿using System;
using System.Text;

namespace AsposeFileLibrary.Common.Helpers
{
    public static class ExtensionHelper
    {
        public static DateTime TryParseToDateTime(this string date)
        {
            var successParse = DateTime.TryParse(date, out DateTime dateTime);

            if(!successParse)
            {
                throw new ArgumentException("The date you provide can`t be parsed to DateTime - " + date);
            }

            return dateTime;
        }

        public static int TryParseToInt(this string integer)
        {
            var successParse = int.TryParse(integer, out int value);

            if (!successParse)
            {
                throw new ArgumentException("The date you provide can`t be parsed to DateTime - " + integer);
            }

            return value;
        }

        public static string ConvertToString(this byte[] binary)
        {
            return Encoding.Default.GetString(binary);
        }
    }
}
