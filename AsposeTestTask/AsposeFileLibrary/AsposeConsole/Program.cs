﻿using System;
using static AsposeConsole.Common.Constants;
using AsposeConsole.Common;

namespace AsposeConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var processor = new Processor();

            Console.WriteLine(MainTitle);

            while (true)
            {
                Console.WriteLine(Choose);
                Console.WriteLine(Read);
                Console.WriteLine(Modify);
                Console.WriteLine(Constants.Convert);

                var value = Console.ReadLine();

                processor.Process(value);
            }
        }
    }
}
