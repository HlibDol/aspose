﻿namespace AsposeConsole.Common
{
    public static class Constants
    {
        public const string MainTitle = "Hi there! This is an additinal app for my test library. Press 0 to return at the beginning.";
        public const string Constructed = "Object is constructed, press 1 to add one more, 2 to modify file";
        public const string Choose = "Please, choose which operation you want to make";
        public const string FileName = "Enter file name(local)";
        public const string FileType = "Choose the read type";
        public const string Remote = "3.Remote url (not tested)";
        public const string Embedded = "1.Embedded";
        public const string Local = "2.Local";
        public const string ModifyType = "Choose modification type:";
        public const string Add = "1.Add";
        public const string Delete = "3.Delete";
        public const string CreateNewIns = "Create new car object:";
        public const string FileModifyed = "Your file was modifyed";
        public const string ConvertFrom = "Convert from:";
        public const string ConvertTo = "Convert to:";
        public const string Xml = "1.xml";
        public const string Binary = "2.binary";
        public const string WasRead = "Your file was read and saved to buffer";
        public const string Read = "1.Read";
        public const string Modify = "2.Modify";
        public const string Convert = "3.Convert";
        public const string Null = "0";
        public const string One = "1";
        public const string Two = "2";
        public const string Three = "3";
        public const string AddObject = "Provide an object";
        public const string Name = "Name:";
        public const string Price = "Price:";
        public const string Date = "Date (DD.MM.YYYY format)";
    }
}
