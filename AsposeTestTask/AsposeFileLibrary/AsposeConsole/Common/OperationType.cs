﻿namespace AsposeConsole.Common
{
    public enum OperationType
    {
        Wait = 0,
        Read = 1,
        Modify = 2,
        Convert = 3
    }
}
