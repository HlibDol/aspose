﻿using AsposeConsole.Abstract;
using AsposeConsole.Components;
using static AsposeConsole.Common.Constants;

namespace AsposeConsole
{
    public class Processor : IProcessor
    {
        private readonly IConvertor _convertor = null;
        private readonly IModifyer _modifyer = null;
        private readonly IReader _reader = null;

        public Processor()
        {
            _reader = new Reader();
            _modifyer = new Modifyer(_reader);
        }

        public void Process(string lineValue)
        {
            if (lineValue == One)
            {
                _reader.ReadAndChooseReadType();
            }
            else if (lineValue == Two)
            {
                _modifyer.Modify();
            }
            else if (lineValue == Three)
            {
                _convertor.Convert();
            }
        }
    }
}
