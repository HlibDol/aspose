﻿using AsposeFileLibrary.Files.Models;

namespace AsposeConsole.Abstract
{
    public interface IReader
    {
        (byte[], FileFormat?) ReadAndChooseReadType();
    }
}
