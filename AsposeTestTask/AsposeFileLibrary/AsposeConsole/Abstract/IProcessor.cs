﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeConsole.Abstract
{
    public interface IProcessor
    {
        void Process(string lineValue);
    }
}
