﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsposeConsole.Abstract
{
    public interface IConvertor
    {
        byte[] Convert();
    }
}
