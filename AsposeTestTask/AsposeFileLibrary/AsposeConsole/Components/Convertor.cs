﻿using AsposeConsole.Abstract;
using AsposeFileLibrary.Files.Aspose;
using AsposeFileLibrary.Files.Models;

namespace AsposeConsole.Components
{
    public class Convertor : IConvertor
    {
        private readonly IReader _reader = null;

        public Convertor(IReader reader)
        {
            _reader = reader;
        }

        public byte[] Convert()
        {
            var bytes = _reader.ReadAndChooseReadType();
            var to = FileFormat.Binary;

            if (bytes.Item2 == FileFormat.Binary)
            {
                to = FileFormat.Xml;
            }

            var aspose = AsposeCommander.Instance;
            bytes.Item1 = aspose.ConvertFileFormat(bytes.Item2.Value, to, bytes.Item1);

            return bytes.Item1;
        }
    }
}
