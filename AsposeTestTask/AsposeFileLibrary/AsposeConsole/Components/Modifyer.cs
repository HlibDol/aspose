﻿using AsposeConsole.Abstract;
using AsposeConsole.Common;
using AsposeFileLibrary.Files.Aspose;
using AsposeFileLibrary.Files.Models;
using System;
using System.Collections.Generic;
using static AsposeConsole.Common.Constants;

namespace AsposeConsole.Components
{
    public class Modifyer : IModifyer
    {
        private readonly IReader _reader = null;

        public Modifyer(IReader reader)
        {
            _reader = reader;
        }

        public byte[] Modify()
        {
            var bytes = _reader.ReadAndChooseReadType();

            Console.WriteLine(ModifyType);
            Console.WriteLine(Add);
            Console.WriteLine(Constants.Modify);
            Console.WriteLine(Delete);

            var consoleLine = Console.ReadLine();

            if (consoleLine == One)
            {
                bytes.Item1 = AddObj(bytes.Item1, bytes.Item2.Value);
            }
            else if (consoleLine == Two)
            {
                bytes.Item1 = ModifyObj(bytes.Item1, bytes.Item2.Value);
            }
            else if (consoleLine == Three)
            {
                bytes.Item1 = DeleteObj(bytes.Item1, bytes.Item2.Value);
            }
            else if (consoleLine == Null)
            {
                return null;
            }
            else
            {
                Console.WriteLine("Wrong format.");

                return null;
            }

            return bytes.Item1;
        }

        private byte[] AddObj(byte[] bytes, FileFormat format)
        {
            var cars = ConstructCars(new List<Car>());

            if (cars == null)
            {
                return null;
            }

            var aspose = AsposeCommander.Instance;

            return aspose.ModifyFile(bytes, cars, format, ModificationType.Add);
        }

        private List<Car> ConstructCars(List<Car> cars)
        {
            Console.WriteLine(AddObject);

            Console.WriteLine(Name);
            var consoleName = Console.ReadLine();

            Console.WriteLine(Price);
            var consolePrice = Console.ReadLine();

            Console.WriteLine(Date);
            var consoleDate = Console.ReadLine();

            var success = int.TryParse(consolePrice, out int price);

            var successDate = DateTime.TryParse(consoleDate, out DateTime dateTime);

            if (!success)
            {
                Console.WriteLine("Price or Date was in wrong format, try again.");

                return null;
            }

            var car = new Car
            {
                BrandName = consoleName,
                Price = price,
                Date = dateTime,
            };

            cars.Add(car);

            Console.WriteLine(Constructed);

            var input = Console.ReadLine();

            if (input.Equals(One))
            {
                ConstructCars(cars);
            }
            else if (input.Equals(Two))
            {
                return cars;
            }

            return null;
        }

        private byte[] ModifyObj(byte[] bytes, FileFormat format)
        {
            var cars = ConstructCars(new List<Car>());

            if (cars == null)
            {
                return null;
            }

            var aspose = AsposeCommander.Instance;

            return aspose.ModifyFile(bytes, cars, format, ModificationType.Modify);
        }

        private byte[] DeleteObj(byte[] bytes, FileFormat format)
        {
            var cars = ConstructCars(new List<Car>());

            if (cars == null)
            {
                return null;
            }

            var aspose = AsposeCommander.Instance;

            return aspose.ModifyFile(bytes, null, format, ModificationType.Delete);
        }
    }
}
