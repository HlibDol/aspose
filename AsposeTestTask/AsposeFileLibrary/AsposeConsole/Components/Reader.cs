﻿using AsposeConsole.Abstract;
using AsposeFileLibrary.Files.Aspose;
using AsposeFileLibrary.Files.Models;
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using static AsposeConsole.Common.Constants;

namespace AsposeConsole.Components
{
    public class Reader : IReader
    {
        public (byte[], FileFormat?) ReadAndChooseReadType()
        {
            string path = null;
            ReadType readType = ReadType.Embedded;

            Console.WriteLine(FileType);
            Console.WriteLine(Embedded);
            Console.WriteLine(Local);

            var consoleLine = Console.ReadLine();

            if (!(consoleLine.Equals(One) || consoleLine.Equals(Two) || consoleLine.Equals(Null)))
            {
                Console.WriteLine("Wrong read type, re-enter");

                ReadAndChooseReadType();
            }

            if (consoleLine == One)
            {
                readType = AsposeFileLibrary.Files.Models.ReadType.Embedded;
                path = GetEmbeddedFile();
            }
            else if (consoleLine == Two)
            {
                readType = AsposeFileLibrary.Files.Models.ReadType.Local;
                Console.WriteLine(FileName);
                path = Console.ReadLine();
            }
            else if (consoleLine == Null)
            {
                return (null, null);
            }

            var bytes = Read(path, readType);

            Task.WaitAll(bytes);

            return (bytes.Result.Item1, bytes.Result.Item2);
        }

        private string GetEmbeddedFile()
        {
            var executingAssembly = Assembly.GetExecutingAssembly();

            var array = executingAssembly
                .GetManifestResourceNames()
                .Where(r => r.EndsWith(".xml") || r.EndsWith(".binary"))
                .ToArray();

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine($"{i + 1}. {array[i]}");
            }

            Console.WriteLine("Choose one");

            var index = Console.ReadLine();
            var success = int.TryParse(index, out int result);

            if (!success)
            {
                Console.WriteLine("Try again");
                GetEmbeddedFile();
            }

            return array[result - 1];
        }

        private async Task<(byte[], FileFormat?)> Read(string path, ReadType readType)
        {
            FileFormat fileFormat;

            if (path.EndsWith(".xml"))
            {
                fileFormat = FileFormat.Xml;
            }
            else if (path.EndsWith(".binary"))
            {
                fileFormat = FileFormat.Binary;
            }
            else
            {
                Console.WriteLine("Wrong file format");

                return (null, null);
            }

            var aspose = AsposeCommander.Instance;
            var bytes = await aspose.ReadFile(path, readType, fileFormat, this);

            return (bytes, fileFormat);
        }
    }
}
